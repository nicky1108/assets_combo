Combo Script - 自动合并/压缩脚本 
 
 * 参考拔赤写的方法，进行改良
 * License: http://www.opensource.org/licenses/mit-license.php

脚本使用:
 - 要求php5及以上版本
 - 程序在找不到本地文件的情况下，会去指定的cdn上找同名文件
 - 这里提供cb.php，用来实现tbcdn的开发环境的模拟,apache的配置在cb.php中
 - 配合KISSY框架使用。

模拟淘宝CDN 
 - http://a.tbcdn.cn/??1.js,2.js
 - http://a.tbcdn.cn/subdir/??1/js,2.js
 

文件列表：
 - combo.php 合并文件，不压缩
 - cb.php 淘宝CDN合并文件策略的模拟

php文件编码统一使用utf-8


